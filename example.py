# %%
from pathlib import Path
from datetime import datetime

import intake
#import sqlalchemy
import pandas
from matplotlib import pyplot


TODAY = datetime.today().strftime("%Y%m%d")
outpaths = [
    Path("01-prepared", d)
    for d in ("latest", TODAY)
]
for d in outpaths:
    d.mkdir(parents=True, exist_ok=True)

cat = intake.open_catalog("example_catalog.yml")



# %%  From Catalog: Read from postgres
timeseries_df = cat.ts_example.read()
print(timeseries_df.head())
for d in outpaths:
    timeseries_df.to_csv(d / "ts_example.csv")
# ------------




# %%  From Catalog: Read from postgres
postgres_df = cat.xf_groups.read()
print(postgres_df.head())
for d in outpaths:
    postgres_df.to_csv(d / "postgres_groups.csv")
# ------------





# %% From Catalog: Read a local set of CSV files
_state_cols = ["state", "code", "admission_number", "capital_city", "population"]
state_df = (
    cat.states.read()
       .sort_values(by="population_rank")
       .loc[:, _state_cols]
)
fig, ax = pyplot.subplots(figsize=(7, 3))
state_df.head(10).plot.bar(x="code", y="population", ax=ax)

for d in outpaths:
    state_df.to_pickle(d / 'stats.pkl')
    fig.savefig(d / "top10_states.png")
# ------------


# %%  From Catalog: Read a parquet file on S3
taxis = cat.nyc_taxi.read()
avg_dist = (
    taxis.groupby(
        [pandas.Grouper(key="tpep_pickup_datetime", freq='1M'), "passenger_count"]
    )['trip_distance'].mean()
)
for d in outpaths:
    avg_dist.to_csv(d / "avg_taxi_dist.csv")
# ------------


# %% "Manually" read our postgres DB
user = password = database = "<fill-in>"
host = "localhost"
port = 5432
engine = sqlalchemy.create_engine(
    str.format('postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}', user, password, host, port, database)
)
query = "select * from model_dictionary limit 100"
src = intake.open_sql(engine, query)
print(src.discover())
print(src.read())
# ------------
